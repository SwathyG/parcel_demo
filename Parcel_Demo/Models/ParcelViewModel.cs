﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Parcel_Demo.Models
{
    public class ParcelViewModel
    {
        public int Id { get; set; }
        public string location_main { get; set; }
        public string location_sending { get; set; }
        public int postcode { get; set; }
        public int weight { get; set; }
        public int length { get; set; }
        public int breadth { get; set; }
        public int height { get; set; }

    }
}
