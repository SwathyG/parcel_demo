﻿using Parcel_Demo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Parcel_Demo.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        ParcelTestingEntities db = new ParcelTestingEntities();

        public ActionResult Index()
        {
            ParcelTestingEntities db = new ParcelTestingEntities();

            return View();

        }
        public ActionResult Save(string mainLocation, string locationSending, int postcode,int length,int breadth, int height, int weight)
        {
            try
            {
                //var objcheck = db.parcel_service
                                  //  .Where(O => O.ExcerciserName == name &&
                                  //  O.ExcerciseDate == date).Any();

                //Data Validation
                 if (weight < 1 || weight > 120)
                {
                  var responseText = "";
                //    responseText += (name == null) ? "Enter name, " : "Enter ";
                //    responseText += (date == null) ? " exercise date, " : "";
                  responseText += (weight < 1 ||
                                   weight > 25) ? "valid Weight, " : "";
                //    responseText += (objcheck) ? "record name only once a day." : ".";
                  return Json(new { Success = false, responseText = responseText });
                }
                var parcelValue = new Parcel_Demo.Models.parcel_service();
                parcelValue.location_main = mainLocation;
                parcelValue.location_sending = locationSending;
                parcelValue.postcode = postcode;
                parcelValue.length = length;
                parcelValue.breadth = breadth;
                parcelValue.height = height;
                parcelValue.weight = weight;
                db.parcel_service.Add(parcelValue);
                db.SaveChanges();

              return Json(db.parcel_service.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw new Exception("Save failed!");
            }
            //return RedirectToAction("Index");
        }
    }
}