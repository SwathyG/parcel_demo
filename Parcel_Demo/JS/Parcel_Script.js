﻿
function myReadonlyFunction() {
    $("#price").attr('readOnly', true);
}


function Save() {

    var mainLocation = document.getElementById("main_location").value;
    var locationSending = document.getElementById("location_sendin").value;
    var postcode = document.getElementById("post_code").value;
    var length = document.getElementById("length").value;
    var breadth = document.getElementById("breadth").value;
    var height = document.getElementById("height").value;
    var weight = document.getElementById("weight").value;

    var error1 = $(".Error1");
    var error2 = $(".Error2");
    var error3 = $(".Error3");

        $.ajax({
            type: "POST",
            url: "/Home/Save",
            dataType: "json", // the URL of the controller action method
            data: {
                mainLocation: mainLocation, locationSending: locationSending, postcode: postcode,
                length: length, breadth: breadth, height: height, weight: weight
            },
            success: function (data) {
                swal({
                    title: "Good job!",
                    text: "Saved..!",
                    icon: "success",
                    button: "Ok!",
                });

                $('#myForm')[0].reset();
            },
            error: function (req, status, error) {
                // do something with error
            }
        });
}


